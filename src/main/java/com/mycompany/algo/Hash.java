/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.algo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author NonWises
 */
public class Hash {
    private int size;
    private ArrayList<String> table = new ArrayList<>();
    
    public Hash(int size){
        this.size = size;
        for (int i = 0; i < size; i++) {
            table.add(i, null);
        }
    }
    
    private int hashFunction(int k){
        return k % size;
    }
    
    public void put(int k, String text){
        int h = hashFunction(k);
        table.set(h, text);
        
       // System.out.println(h);
        
    }
    
    public void get(int k){
        int h = hashFunction(k);
        System.out.println(table.get(h));
    }
    
    public void getAll(){
        for (int i = 0; i < size; i++) {
            System.out.println("index: "+i+", value: "+table.get(i));
        }
    }
    
    public void remove(int k){
        int h = hashFunction(k);
        //System.out.println(h);
        table.set(h, null);
    }
    
    
}
