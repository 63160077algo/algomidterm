/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.algo;

/**
 *
 * @author NonWises
 */
public class Node {
    int key;
    String value;

    Node(int key, String value) {
        this.key = key;
        this.value = value;
    }

}
