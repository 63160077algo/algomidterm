/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.algo;

import java.util.Scanner;

/**
 *
 * @author NonWises
 */
public class intParse {
    
    public static int parse(String s){
        int result=0;
        for (int i = 0; i < s.length(); i++) {
            //from horner rule ex 123 = 1*10^2 + 2*10^1 + 3*10^0
            result += Character.getNumericValue(s.charAt(i))*Math.pow(10, s.length()-i-1);
        }
        
        return result;
    }
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String text = kb.next();
        System.out.println(parse(text));
        kb.close();
    }
}
