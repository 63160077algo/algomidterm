/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.algo;

import java.awt.List;
import java.util.Scanner;

/**
 *
 * @author NonWises
 */
public class reverseArray {
    public static void reverse(int[] A){
        int temp; // initial temporary variable
        for (int i = 0; i < A.length/2; i++) {
            temp = A[i]; //let temp contain A[i]
            A[i] = A[A.length-i-1];//set A[i] with 
            A[A.length-i-1] = temp;//set A[A.length-i-1] with temp 
        }
        
    }
    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int n = kb.nextInt();
        int[] A = new int[n];
        for (int i = 0; i < A.length; i++) {
            A[i] = kb.nextInt();
            
        }
        reverse(A);
        for (int i : A) {
            System.out.print(i+" ");
        }
        
        kb.close();
    }
}
